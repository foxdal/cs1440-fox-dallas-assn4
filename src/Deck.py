import sys
import Card


class Deck():
    """Deck constructor"""
    def __init__(self, cardSize, cardCount, numberMax):
        self.__cardSize = cardSize
        self.__cardCount = cardCount
        self.__numberMax = numberMax
        self.__cards = []
        for i in range(self.__cardCount):
            self.__cards.append(Card.Card(i, cardSize, numberMax))


    """Return an integer: the number of cards in this deck"""
    def getCardCount(self):
        return self.__cardCount


    """Return card N from the deck"""
    def getCard(self, n):
        card = None
        n -= 1
        if 0 <= n < self.getCardCount():
            card = self.__cards[n]
        return card


    """void function: Print cards from the Deck
    If an index is given, print only that card.
    Otherwise, print each card in the Deck"""
    def print(self, file=sys.stdout, idx=None):
        if idx is None:
            for idx in range(1, self.__cardCount + 1):
                c = self.getCard(idx)
                c.print(file)
            print('', file=file)
        else:
            self.getCard(idx).print(file)
