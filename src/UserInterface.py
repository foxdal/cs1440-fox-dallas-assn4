import Deck
import Menu


class UserInterface():
    def __init__(self):
        self.__currentDeck = None
        self.__size = 0
        self.__maxNum = 0
        self.__numCards = 0


    """Present the main menu to the user and repeatedly prompt for a valid command"""
    def run(self):
        print("Welcome to the Bingo! Deck Generator\n")
        menu = Menu.Menu("Main")
        menu.addOption("C", "Create a new deck")
        
        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "C":
                self.__currentDeck = self.__createDeck()
                self.__deckMenu()
            elif command == "X":
                keepGoing = False


    """Command to create a new Deck"""
    def __createDeck(self):

        keepGoing = True
        while keepGoing:
            size = eval(input("Enter card size (3 - 15):\n"))
            if 3 <= size <= 15:
                self.__size = size
                keepGoing = False
            else:
                print()
                print("<INVALID INPUT> - Please enter a number within the provided range\n")

        keepGoing2 = True
        while keepGoing2:
            maxNum = eval(input("Enter max number (" + str(2 * (self.__size ** 2)) + " - " + str(4 * (self.__size ** 2)) + "):\n"))
            if (2 * (self.__size ** 2)) <= maxNum <= (4 * (self.__size ** 2)):
                self.__maxNum = maxNum
                keepGoing2 = False
            else:
                print()
                print("<INVALID INPUT> - Please enter a number within the provided range\n")

        keepGoing3 = True
        while keepGoing3:
            numCards = eval(input("Enter number of cards (3 - 10000):\n"))
            if 3 <= numCards <= 10000:
                self.__numCards = numCards
                keepGoing3 = False
            else:
                print()
                print("<INVALID INPUT> - Please enter a number within the provided range\n")

        deck = Deck.Deck(self.__size, self.__numCards, self.__maxNum)
        return deck


    """Present the deck menu to user until a valid selection is chosen"""
    def __deckMenu(self):
        menu = Menu.Menu("Deck")
        menu.addOption("P", "Print a card to the screen")
        menu.addOption("D", "Display the whole deck to the screen")
        menu.addOption("S", "Save the whole deck to a file")

        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "P":
                self.__printCard()
            elif command == "D":
                print()
                self.__currentDeck.print()
            elif command == "S":
                self.__saveDeck()
            elif command == "X":
                keepGoing = False


    """Command to print a single card"""
    def __printCard(self):
        keepGoing = True
        while keepGoing:
            cardToPrint = eval(input("Id of card to print (1 - " + str(self.__currentDeck.getCardCount()) + "): "))
            if 0 < cardToPrint <= self.__currentDeck.getCardCount():
                print()
                self.__currentDeck.print(idx=cardToPrint)
                keepGoing = False
            else:
                print()
                print("<INVALID INPUT> - Please enter a number within the provided range\n")


    """Command to save a deck to a file"""
    def __saveDeck(self):
        fileName = input("Enter output file name: ")
        if fileName != "":
            outputStream = open(fileName, 'w')
            self.__currentDeck.print(outputStream)
            outputStream.close()
            print("Done!")
