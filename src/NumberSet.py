import random


class NumberSet():
    """NumberSet constructor"""
    def __init__(self, size):
        self.__size = int(size)
        self.__numbers = []
        self.__current = -1
        for i in range(1, self.__size + 1):
            self.__numbers.append(i)


    """Return an integer: the size of the NumberSet"""
    def getSize(self):
        return self.__size


    """Return an integer: get the number from this NumberSet at an index"""
    def get(self, index):
        if self.__size == 0:
            return None
        return self.__numbers[index]


    """void function: Shuffle this NumberSet"""
    def randomize(self):
        random.shuffle(self.__numbers)


    """Return an integer: when called repeatedly return successive values
    from the NumberSet until the end is reached, at which time 'None' is returned"""
    def getNext(self):
        if self.__current < (len(self.__numbers) - 1):
            self.__current += 1
            return self.__numbers[self.__current]
        else:
            return None


    def reset(self):
        self.__current = -1
