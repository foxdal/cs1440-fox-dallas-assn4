import sys
import NumberSet


class Card():
    """Card constructor"""
    def __init__(self, idnum, size, maxNum):
        self.__idnum = idnum
        self.__size = size
        self.__numberSet = NumberSet.NumberSet(maxNum)
        self.__numberSet.randomize()


    """Return an integer: the ID number of the card"""
    def getId(self):
        return self.__idnum


    """Return an integer: the size of one dimension of the card.
    A 3x3 card will return 3, a 5x5 card will return 5, etc."""
    def getSize(self):
        return self.__size


    """void function:
    Prints a card to the screen or to an open file object"""
    def print(self, file=sys.stdout):
        # If statement to check if size of card is odd (in order to incorporate "FREE!" square)
        if (self.__size % 2) == 1:
            file.write("\n")
            file.write("Card #" + str(self.__idnum + 1))
            for i in range(0, self.__size):
                file.write("\n+") + (file.write("-----+" * self.__size))
                file.write("\n")
                file.write("|")
                if i == (self.__size // 2):
                    for j in range(0, (self.__size // 2)):
                        toPrint = '{:^5s}'.format(str(self.__numberSet.getNext()))
                        file.write(toPrint)
                        file.write("|")
                    file.write("FREE!|")
                    for h in range(0, (self.__size // 2)):
                        toPrint = '{:^5s}'.format(str(self.__numberSet.getNext()))
                        file.write(toPrint)
                        file.write("|")
                else:
                    for k in range(0, self.__size):
                        toPrint = '{:^5s}'.format(str(self.__numberSet.getNext()))
                        file.write(toPrint)
                        file.write("|")

            file.write("\n+") + (file.write("-----+" * self.__size))
            file.write("\n")

        else:
            file.write("\n")
            file.write("Card #" + str(self.__idnum + 1))
            for i in range(0, self.__size):
                file.write("\n+") + (file.write("-----+" * self.__size))
                file.write("\n")
                file.write("|")
                for k in range(0, self.__size):
                    toPrint = '{:^5s}'.format(str(self.__numberSet.getNext()))
                    file.write(toPrint)
                    file.write("|")

            file.write("\n+") + (file.write("-----+" * self.__size))
            file.write("\n")

        self.__numberSet.reset()
