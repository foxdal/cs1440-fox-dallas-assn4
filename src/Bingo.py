#!/usr/bin/python3
"""
Dallas Fox
CS - 1440 - 001
Assn 4 - Bingo!
"""

import UserInterface

# Create a UI object and run it
ui = UserInterface.UserInterface()
ui.run()
