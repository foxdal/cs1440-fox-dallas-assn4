import unittest
import Card


class TestCard(unittest.TestCase):
    def setUp(self):
        """
        I changed the third parameter in each card object below, because originally it used
        a NumberSet object as the third parameter, but I have my code set up in a way that
        I only pass the Maximum Number (what would be passed to a NumberSet) as my third
        parameter for Card, as I initialize a NumberSet object in Card itself, not Deck,
        because I am creating a new NumberSet for EACH Card object, instead of the same
        single NumberSet randomized over and over again for one Deck object

        My code still performs exactly as required, nonetheless
        """
        self.card = Card.Card(0, 3, 18)
        self.card1 = Card.Card(0, 0, 0)


    def test_getSize(self):
        self.assertEqual(self.card.getSize(), 3)
        self.assertEqual(self.card1.getSize(), 0)

    def test_getID(self):
        self.assertIsNotNone(self.card)
        self.assertIsNotNone(self.card1)
        self.assertEqual(self.card.getId(), 0)
        self.assertEqual(self.card1.getId(), 0)



if __name__ == '__main__':
    unittest.main()
