import MenuOption


class Menu():
    """Menu constructor"""
    def __init__(self, header):
        self.__header = header
        self.__optionCount = 0
        self.__options = []


    """Add an option to the menu"""
    def addOption(self, command, description):
        if command is not None and command != "":
            self.__options.append(MenuOption.MenuOption(command, description))
            self.__optionCount += 1


    """Check that a command is contained in our list of menu options"""
    def __isValidCommand(self, command):
        isValid = False
        if command == "X":
            isValid = True
        else:
            for i in range(self.getOptionCount()):
                if command == self.getOption(i).getCommand():
                    isValid = True
                    break
        return isValid


    def getOption(self, optionIndex):
        option = None
        if optionIndex >= 0 and optionIndex < self.getOptionCount():
            option = self.__options[optionIndex]
        return option


    def getHeader(self):
        return self.__header


    def getOptionCount(self):
        return self.__optionCount


    """Display the menu and take a command from the user"""
    def show(self):
        command, keepGoing = '', True

        while keepGoing:
            optionList = ''

            print()
            print(self.getHeader(), "menu:")

            for i in range(self.getOptionCount()):
                option = self.getOption(i)
                if option is not None:
                    # 1st field is 6 chars wide
                    print(f"{option.getCommand()} - {option.getDescription()}")
                    optionList += option.getCommand() + ", "

            print("X - Exit")
            optionList += "X"

            print(f"\nEnter a {self.getHeader()} command ({optionList})")
            command = input()
            keepGoing = not self.__isValidCommand(command)

        return command
