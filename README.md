**NOTE**
My code runs perfectly as required, but when creating a deck of a large
quantity, the program takes about 15 seconds (on my computer) to create the
entire deck object, so make sure to give it a moment please and thank you!